#include "../include/hmi_socket.h"

hmiSocket::hmiSocket( int port, string sem_name ) : WebSocketServer( port )
{
  this->sem = sem_open(sem_name.c_str(), 0);
}

hmiSocket::~hmiSocket( )
{
  sem_close(this->sem);
  sem_unlink("on_message_s");
}


void hmiSocket::onConnect( int socketID )
{
  if (socket_id == -1){
    socket_id = socketID;
  }
  Util::log( "New connection on socket ID " + to_string(socket_id));
}

void hmiSocket::onMessage( int socketID, const string& data )
{
  Util::log( "Received: " + data );
  message_q.push(data);
  sem_post(this->sem);
}

void hmiSocket::onDisconnect( int socketID )
{
  Util::log( "Disconnect" );
  socket_id = -1;
}

void hmiSocket::onError( int socketID, const string& message )
{
  Util::log( "Error: " + message );
}

bool hmiSocket::is_init(){
  return (socket_id != -1);
}

int hmiSocket::get_socket_id(void){
  return socket_id;
}

bool hmiSocket::check_message(void){
  return !message_q.empty();
}

string hmiSocket::get_message(void){
  string ret_value = string("");
  if(!message_q.empty()){
    ret_value = message_q.front();
    message_q.pop();
  }
  else{
    Util::log("Message queue is empty");
    ret_value = string("");
  }
  return ret_value;
}

void hmiSocket::send_message(const string &message){
  this->send(socket_id, message);
  Util::log( "Send message " + message );
}
