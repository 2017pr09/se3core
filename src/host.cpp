#include "../include/host.h"

static int run = 1;

using namespace std;

static se3interface *secube = NULL;
static mqtt_broker *local_broker = NULL;
static hmiSocket *hmi = NULL;

static sem_t *sem;

// mqtt control topics (host -> client)
static vector<string> control_topic;
// mqtt status topics (client -> host)
static vector<string> status_topic;

void handle_signal(int s)
{
	# if LOG_LEVEL > LOG_LEVEL_NO
  	cout << "LOG: Received signal " << (s == 15 ? "SIGTERM" : "SIGINT") << endl;
		cout << "LOG: shutting down se3core_host..." << endl;
	#endif
  sem_t *stop_s = sem_open(SEM_NAME, 0);
  sem_post(stop_s);
  sem_close(stop_s);
  sem_unlink(SEM_NAME);
	run = 0;
}

void *on_message_f(void *params)
{
  int ret_val = 0;
  string message;
	char *msg_str;
	char *msg;
	unsigned int topic_idx = -1;
	uint8_t *enc_buffer = 0;
	uint16_t enc_buffer_len = 0;

  while(run){
    sem_wait(sem);
    if(hmi != NULL){
      if(hmi->is_init()){
        while(hmi->check_message()){
					message = hmi->get_message();
					msg_str = (char*) malloc(sizeof(char)*message.length());

					strcpy(msg_str, message.c_str());
					msg = strtok(msg_str, "@");
				  topic_idx = strtol(strtok(NULL, "@"), NULL, 10);
					if((topic_idx >= 0) && (topic_idx < control_topic.size())){
						# if LOG_LEVEL > LOG_LEVEL_NO
							cout << "LOG: sending message " + string(msg) + " to topic " + control_topic.at(topic_idx) << endl;
						#endif
						enc_buffer = secube->encrypt_buffer((uint8_t *) msg, strlen(msg), &enc_buffer_len);
						if(enc_buffer_len > 0){
				    	local_broker->broker_publish(enc_buffer, enc_buffer_len, control_topic.at(topic_idx));
						}
					}
					else{
						# if LOG_LEVEL > LOG_LEVEL_NO
							cout << "LOG: invalid topic index" << endl;
						#endif
					}
					free(msg_str);
					usleep(10000);	// avoiding to overload socket
        }
      }
    }
  }
  sem_close(sem);

	# if LOG_LEVEL > LOG_LEVEL_NO
  	cout << "Exiting receiving thread" << endl;
	#endif
  pthread_exit(& ret_val);
}

void *socket_f(void *params)
{
  int ret_val = 0;
	# if LOG_LEVEL > LOG_LEVEL_NO
		cout << "LOG: hmiSocket running on port " << (int) HMI_SERVER_PORT << endl;
	#endif

  while(run){
    hmi->wait(TIMEOUT);
  }

	# if LOG_LEVEL > LOG_LEVEL_NO
		cout << "Exiting socket thread" << endl;
	#endif
  pthread_exit(& ret_val);
}

int main(int argc, char *argv[])
{
	signal(SIGINT, handle_signal);
	signal(SIGTERM, handle_signal);

	pthread_t t_receiver, t_server;

	control_topic.push_back(string("control0"));
	control_topic.push_back(string("control1"));
	control_topic.push_back(string("control2"));

	status_topic.push_back(string("status0"));
	status_topic.push_back(string("status1"));
	status_topic.push_back(string("status2"));


	sem = sem_open(SEM_NAME, O_CREAT, 0777, 0);

	hmi_socket_init();
  mqtt_init();
	secube_init();

	if(pthread_create(&t_receiver, NULL, on_message_f, NULL)) {
		# if LOG_LEVEL > LOG_LEVEL_NO
    	cout << "Error creating receiving thread" << endl;
		#endif
  }

  if(pthread_create(&t_server, NULL, socket_f, NULL)) {
		# if LOG_LEVEL > LOG_LEVEL_NO
    	cout << "Error creating socket thread" << endl;
		#endif
  }

  if(pthread_join(t_receiver, NULL)) {
		# if LOG_LEVEL > LOG_LEVEL_NO
    	cout << "Error joining receiving thread" << endl;
		#endif
  }

  if(pthread_join(t_server, NULL)) {
		# if LOG_LEVEL > LOG_LEVEL_NO
    	cout << "Error joining socket thread" << endl;
		#endif
  }
	local_broker->broker_stop();

	delete local_broker;
	delete secube;
	delete hmi;

	sem_close(sem);
  sem_unlink(SEM_NAME);

	control_topic.clear();
	status_topic.clear();

	return 0;
}

bool mqtt_init(void){
  bool ret_value = true;

  local_broker = new mqtt_broker(MQTT_BROKER_ID_HOST, string(MQTT_BROKER_NAME_HOST), string(MQTT_BROKER_IP_HOST), MQTT_BROKER_PORT_HOST);

	local_broker->broker_init();
	local_broker->broker_set_message_callback(mqtt_message_callback);
	local_broker->broker_connect();

	for(vector<string>::iterator it = status_topic.begin(); it != status_topic.end(); it++){
		local_broker->broker_subscribe(*it);
	}

	local_broker->broker_run();

  return ret_value;
}

bool secube_init(void){
	bool ret_value = false;

  secube = new se3interface(0);

  if(secube != NULL){
		#if VIRTUAL_CUBE == 1
    	ret_value = secube->interface_init(true);
		#else
			ret_value = secube->interface_init(false);
		#endif
  }

  if(ret_value){
    secube->set_pin_admin(pin_admin);
    secube->set_pin_user(pin_user);
    secube->set_data_key_1(data_key_1);
    secube->set_data_key_2(data_key_2);
    secube->set_data_key_3(data_key_3);

    secube->print_pin_admin();
    secube->print_pin_user();
    secube->print_data_key_1();
    secube->print_data_key_2();
    secube->print_data_key_3();

		secube->login_user();
		secube->set_time();
  }
  return ret_value;
}

bool hmi_socket_init(void){
	bool ret_value = false;
	hmi = new hmiSocket(HMI_SERVER_PORT, string(SEM_NAME));
	if(hmi != NULL){
		ret_value = true;
		# if LOG_LEVEL > LOG_LEVEL_NO
	    cout << "LOG: create new hmiSocket on port " << (int) HMI_SERVER_PORT << endl;
	  #endif
	}
	else{
		# if LOG_LEVEL > LOG_LEVEL_NO
	    cout << "LOG: Failer to create hmiSocket on port " << (int) HMI_SERVER_PORT << endl;
	  #endif
	}
	return ret_value;
}

void mqtt_message_callback(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message)
{
	uint8_t *dec_buffer = NULL;
	uint16_t dec_buffer_len = 0;

  # if LOG_LEVEL > LOG_LEVEL_NO
    cout << "LOG: MQTT got message for topic " << string(message->topic) << endl;
  #endif

	cout << "LOG: Message received: " << (void *) message->payload << endl;
	dec_buffer = secube->decrypt_buffer((uint8_t *) message->payload, message->payloadlen, &dec_buffer_len);

	if(dec_buffer_len > 0){
		cout << "LOG: Decrypted message: " << (void *) dec_buffer << endl;
	}

	// check topic
	for(vector<string>::iterator it = status_topic.begin(); it != status_topic.end(); it++){
		if(string(message->topic) == *it){
			update_hmi(dec_buffer, dec_buffer_len);
		}
	}

	dec_buffer = NULL;
}

void update_hmi(uint8_t *l_dec_buffer, uint16_t l_dec_buffer_len){
	// format -> #zone1#0#0#1#0#2#1
	string message("");

	if (l_dec_buffer_len > 0){
		message = string((char *)l_dec_buffer);
		if(hmi->is_init()){
				hmi->send(hmi->get_socket_id(), message);
		}
	}
	else {
		cout << "LOG: Message is empty" << endl;
	}

}
