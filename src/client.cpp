#include "../include/client.h"

static int run = 1;

using namespace std;

static board *r_pi_board = NULL;
static se3interface *secube = NULL;
static mqtt_broker *local_broker = NULL;

// mqtt control topics (host -> client)
static vector<string> control_topic;
// mqtt status topics (client -> host)
static vector<string> status_topic;

void handle_signal(int s)
{
	# if LOG_LEVEL > LOG_LEVEL_NO
  	cout << "LOG: Received signal " << (s == 15 ? "SIGTERM" : "SIGINT") << endl;
		cout << "LOG: shutting down se3core_client..." << endl;
	#endif
	run = 0;
}

int main(int argc, char *argv[])
{
	signal(SIGINT, handle_signal);
	signal(SIGTERM, handle_signal);

	control_topic.push_back(string("control0"));
	control_topic.push_back(string("control1"));
	control_topic.push_back(string("control2"));

	status_topic.push_back(string("status0"));
	status_topic.push_back(string("status1"));
	status_topic.push_back(string("status2"));

  mqtt_init();
  board_init();
	secube_init();

	while(run){
		sleep(1);
	}

	local_broker->broker_stop();

	delete local_broker;
	delete secube;
  delete r_pi_board;

	control_topic.clear();
	status_topic.clear();

	return 0;
}

bool mqtt_init(void){
  bool ret_value = true;

  local_broker = new mqtt_broker(MQTT_BROKER_ID_CLIENT, string(MQTT_BROKER_NAME_CLIENT), string(MQTT_BROKER_IP_CLIENT), MQTT_BROKER_PORT_CLIENT);

	local_broker->broker_init();
	local_broker->broker_set_message_callback(mqtt_message_callback);
	local_broker->broker_connect();

	for(vector<string>::iterator it = control_topic.begin(); it != control_topic.end(); it++){
		local_broker->broker_subscribe(*it);
	}

	local_broker->broker_run();

  return ret_value;
}

bool board_init(void){
  bool ret_value = true;

  r_pi_board = new board(0, string(BOARD_VENDOR), string(BOARD_MODEL), string(BOARD_SERIAL));
	if(!r_pi_board->board_init()){
    ret_value = false;
  }
	// ground is on pin #09
  r_pi_board->digital_pin_add(0, string("GPIO17"), DPIN_MODE_OUT, DVALUE_LOW);	//pin #11 (GPIO17) GEN_GPIO_0
	r_pi_board->digital_pin_add(1, string("GPIO18"), DPIN_MODE_OUT, DVALUE_LOW);	//pin #12 (GPIO18) GEN_GPIO_1
	r_pi_board->digital_pin_add(2, string("GPIO27"), DPIN_MODE_OUT, DVALUE_LOW);	//pin #13 (GPIO27) GEN_GPIO_2
	r_pi_board->digital_pin_add(3, string("GPIO22"), DPIN_MODE_OUT, DVALUE_LOW);	//pin #15 (GPIO22) GEN_GPIO_3

  return ret_value;
}

bool secube_init(void){
	bool ret_value = false;

  secube = new se3interface(0);

	if(secube != NULL){
		#if VIRTUAL_CUBE == 1
    	ret_value = secube->interface_init(true);
		#else
			ret_value = secube->interface_init(false);
		#endif
  }

  if(ret_value){
    secube->set_pin_admin(pin_admin);
    secube->set_pin_user(pin_user);
    secube->set_data_key_1(data_key_1);
    secube->set_data_key_2(data_key_2);
    secube->set_data_key_3(data_key_3);

    secube->print_pin_admin();
    secube->print_pin_user();
    secube->print_data_key_1();
    secube->print_data_key_2();
    secube->print_data_key_3();

		secube->login_user();
		secube->set_time();
  }
  return ret_value;
}

void mqtt_message_callback(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message)
{
	uint8_t *dec_buffer = NULL;
	uint16_t dec_buffer_len = 0;

  # if LOG_LEVEL > LOG_LEVEL_NO
    cout << "LOG: MQTT got message for topic " << string(message->topic) << endl;
  #endif

	cout << "LOG: Message received: " << (void *) message->payload << endl;
	dec_buffer = secube->decrypt_buffer((uint8_t *) message->payload, message->payloadlen, &dec_buffer_len);

	if(dec_buffer_len > 0){
		cout << "LOG: Decrypted message: " << (void *) dec_buffer << endl;
	}

	for(vector<string>::iterator it = control_topic.begin(); it != control_topic.end(); it++){
		if(string(message->topic) == *it){
			parse_message(dec_buffer, dec_buffer_len);
		}
	}
	dec_buffer = NULL;
}

void parse_message(uint8_t *l_dec_buffer, uint16_t l_dec_buffer_len){
	char *command;
	char *led_id;
	char *led_value;

	if (l_dec_buffer_len > 0){
	  command = strtok((char *)l_dec_buffer, " ");
	  cout << "command : " << string(command) << endl;
		if(string(command) == string("set")){						// format -> set #0#0
			command = strtok(NULL, " ");
		  led_id = strtok(command, "#");
		  cout << "led id : " << string(led_id) << endl;
		  led_value = strtok(NULL, "#");
		  cout << "led value : " << string(led_value) << endl;
			r_pi_board->digital_write((uint8_t) strtol(led_id, NULL, 10) , (bool) strtol(led_value, NULL, 2));
		}
		else if(string(command) == string("toggle")){ 	// format -> toggle #0
			command = strtok(NULL, " ");
		  led_id = strtok(command, "#");
		  cout << "led id : " << string(led_id) << endl;
			r_pi_board->digital_toggle((uint8_t) strtol(led_id, NULL, 10));
		}
		else if(string(command) == string("status")){		// format -> status
			cout << "LED STATUS" << endl;
			update_status();
		}
		else{
			cout << "LOG: INVALID COMMAND" << endl;
		}
	}
	else {
		cout << "LOG: Message is empty" << endl;
	}
	update_status();
	return;
}

void update_status(void){
	uint8_t *enc_buffer = 0;
	uint16_t enc_buffer_len = 0;
	bool read_value = false;

	string message = string("#zone1");

	message += "#0";
	read_value = r_pi_board->digital_read(0);
	message += "#" + to_string(read_value);

	message += "#1";
	read_value = r_pi_board->digital_read(1);
	message += "#" + to_string(read_value);

	enc_buffer = secube->encrypt_buffer((uint8_t *) message.c_str(), message.length(), &enc_buffer_len);
	if(enc_buffer_len > 0){
		local_broker->broker_publish(enc_buffer, enc_buffer_len, status_topic.at(0));
	}

	message.clear();

	message = string("#zone2");

	message += "#2";
	read_value = r_pi_board->digital_read(2);
	message += "#" + to_string(read_value);

	enc_buffer = secube->encrypt_buffer((uint8_t *) message.c_str(), message.length(), &enc_buffer_len);
	if(enc_buffer_len > 0){
		local_broker->broker_publish(enc_buffer, enc_buffer_len, status_topic.at(0));
	}

	message.clear();

	message = string("#zone3");

	message += "#3";
	read_value = r_pi_board->digital_read(3);
	message += "#" + to_string(read_value);

	enc_buffer = secube->encrypt_buffer((uint8_t *) message.c_str(), message.length(), &enc_buffer_len);
	if(enc_buffer_len > 0){
		local_broker->broker_publish(enc_buffer, enc_buffer_len, status_topic.at(0));
	}

}
