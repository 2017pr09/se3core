#include <iostream>
#include <cstdlib>
#include <string>
#include <vector>
#include <thread>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <signal.h>
#include "config.h"
#include "../modules/se3adapter/include/board.h"
#include "../modules/se3mqtt/include/mqtt_broker.h"
#include "../modules/se3interface/include/se3interface.h"

/******************************************************************************/
/******************************MQTT PARAMETERS*********************************/
/******************************************************************************/
// message callback
void mqtt_message_callback(struct mosquitto *mosq, void *obj, const struct mosquitto_message *message);

/******************************************************************************/
/****************************ADAPTER PARAMETERS********************************/
/******************************************************************************/
#define BOARD_VENDOR "Raspberry Pi"
#define BOARD_MODEL "3 rev B"
#define BOARD_SERIAL "00000000"

/******************************************************************************/
/****************************SECUBE PARAMETERS*********************************/
/******************************************************************************/
#define TEST_STRING   ("test_encryption")
#define TEST_SIZE     strlen(TEST_STRING)

static uint8_t pin_admin[32] = {
	'a','d','m','i', 'n',0,0,0, 0,0,0,0, 0,0,0,0,
	0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0
};

static uint8_t pin_user[32] = {
	'u','s','e','r', 0,0,0,0, 0,0,0,0, 0,0,0,0,
	0,0,0,0, 0,0,0,0, 0,0,0,0, 0,0,0,0
};

static uint8_t data_key_1[32] = {
	1,2,3,4, 1,2,3,4, 1,2,3,4, 1,2,3,4,
	1,2,3,4, 1,2,3,4, 1,2,3,4, 1,2,3,4
};

static uint8_t data_key_2[32] = {
	5,6,7,8, 5,6,7,8, 5,6,7,8, 5,6,7,8,
	5,6,7,8, 5,6,7,8, 5,6,7,8, 5,6,7,8
};

static uint8_t data_key_3[32] = {
	0,9,1,8, 0,9,1,8, 0,9,1,8, 0,9,1,8,
	0,9,1,8, 0,9,1,8, 0,9,1,8, 0,9,1,8
};

/******************************************************************************/
/****************************SYSTEM PARAMETERS*********************************/
/******************************************************************************/
#define STATUS_ERROR     -1
#define STATUS_OK         0

/******************************************************************************/
/*****************************INIT FUNCTIONS***********************************/
/******************************************************************************/

bool board_init(void);
bool mqtt_init(void);
bool secube_init(void);

/******************************************************************************/
/***************************MESSAGE FUNCTIONS**********************************/
/******************************************************************************/
void parse_message(uint8_t *l_dec_buffer, uint16_t l_dec_buffer_len);
void update_status(void);
