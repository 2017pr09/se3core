#include <iostream>
#include <string>
#include <queue>
#include <semaphore.h>
#include <pthread.h>
#include "../modules/se3web/include/WebSocketServer.h"
#include "../modules/se3web/include/Util.h"

/******************************************************************************/
/****************************SOCKET PARAMETERS*********************************/
/******************************************************************************/
#define LOG_TO_STDOUT 1

using namespace std;

class hmiSocket : public WebSocketServer
{
  int socket_id = -1;
  sem_t *sem = NULL;
  queue<string> message_q;

public:
  hmiSocket             (    int port, string sem_name           );
  virtual     ~hmiSocket(                                        );
  virtual void onConnect(    int socketID                        );
  virtual void onMessage(    int socketID, const string& data    );
  virtual void onDisconnect( int socketID                        );
  virtual void   onError(    int socketID, const string& message );
  bool           is_init(    void                                );
  int      get_socket_id(    void                                );
  bool     check_message(    void                                );
  string     get_message(    void                                );
  void      send_message(    const string& message               );
};
